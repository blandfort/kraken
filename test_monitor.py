import sys

from twitter.monitor import track_keywords, TestListener


#TODO
# 1) store media in dir with date in name
# 2) log which keywords were followed for obtaining data

if __name__=="__main__":
    import logger
    from keywords import emotional_hashtags

    # NOTE: Hashtags need to be passed in quotes,
    # otherwise the terminal will interpret the hashtag as comment.
    if len(sys.argv)>1:
        keywords = sys.argv[1:]
    else:
        keywords = emotional_hashtags

    print("Following the following keywords:", ', '.join(keywords))
    track_keywords(keywords, listener=TestListener())
