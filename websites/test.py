from bs4 import BeautifulSoup

import requests
import urllib.request
import urllib.parse
import shutil
import sys
import os


# Some code for downloading images from a website
# (Still needs to be put into nice tidy modules)
if __name__=='__main__':
    if len(sys.argv)>1:
        url = sys.argv[1]
    else:
        url = 'https://example.com'

    print("Sending request to '%s' ..."%url)
    response = requests.get(url)

    print("Response received:", response)

    soup = BeautifulSoup(response.text, "html.parser")

    aas = soup.find_all("a")

    #print(aas)
    image_info = []

    for a in aas:
        image_tag = a.findChildren("img")
        if len(image_tag):
            src = image_tag[0]['src']
            fullurl = urllib.parse.urljoin(url, src)
            image_info.append((fullurl, image_tag[0]))
    print("Found %d images!" % len(image_info))

    print("Downloading ...")
    urls = [img[0] for img in image_info]
    base_path = 'temp_imgs/'
    for url in urls:
        print("Downloading %s"%url)
        filename = os.path.join(base_path, url.split('/')[-1])

        if os.path.isfile(filename):  # skip existing files
            continue

        try:
            urllib.request.urlretrieve(url, filename)

            # updating mapping from original urls to file on disk
            with open(config['files']['url_mapping'], 'a') as f:
                f.write('%s\t%s\n' % (url, os.path.abspath(filename)))
        except KeyboardInterrupt:
            break
        except Exception as e:
            # log the exception
            with open(os.path.join(base_path, 'errors.log'), 'a') as f:
                f.write(url+'\n')
                f.write(str(type(e))+'\n')
    print("Finished everything!")
