import scrapy
import urllib
import re
from bs4 import BeautifulSoup


# First compile a list of the links to the people's pages
profile_link_page = 'file:///home/john/code/kraken/files/websites/bundestagsabgeordnete.html'

#NOTE This code is run by executing in the terminal from the main directory:
# scrapy runspider websites/test_spider.py -o path/to/output.json

class BundestagSpider(scrapy.Spider):
    name = 'bundestag'
    start_urls = [
        profile_link_page,
    ]

    def parse(self, response):
        # Identify name
        name_part = response.css('div[class*=bt-biografie-name] h3::text').get()
        if name_part is not None:
            # h3 header has name and party separated by comma
            name, party = name_part.split(',')

            # Initialize dictionary with information about the candidate
            info = {'name': name.strip(), 'party': party.strip()}

            # Extracting profession
            profession = response.css('div[class*=bt-biografie-beruf] p::text').get()
            if profession is not None:
                info['profession'] = profession

            # Extract biographie text
            bio = response.css('div[aria-labelledby*=tab_biografie]')
            if bio is not None:
                info['bio'] = ('\n'.join(bio.css('p::text').getall())).strip()

            landesliste = response.css('div[id*=bt-landesliste-collapse]')
            if landesliste is not None:
                # Extract state
                state = landesliste.css('h5::text').get()
                info['state'] = state

                # Extract wahlkreis
                for linktext in landesliste.css('a::text').getall():
                    if linktext.startswith('Wahlkreis'):
                        info['Wahlkreis'] = linktext
                        break

            # Extract memberships
            amt = response.css('div[id=bt-aemter-collapse]').get()
            if amt is not None:
                mamt = amt.replace('<h5>', '$$$<h5>')
                for text in mamt.split('$$$'):
                    soup = BeautifulSoup(text, 'html.parser')

                    category = soup.find('h5')

                    if category is not None:
                        category = category.text.strip()

                        positions = []
                        for position in soup.find_all('a'):
                            positions.append(position.text.strip())
                        info[category] = positions

            # Finding online profiles
            for linkliste in response.css('div[id*=bt-kontakt-collapse] ul[class*=bt-linkliste]'):
                profiles = {}
                for profile in linkliste.css('a'):
                    title = profile.css('a::attr(title)').get()

                    # For own website the title is the URL
                    if '.' in title:
                        title = 'website'

                    url = profile.css('a::attr(href)').get()
                    profiles[title] = url

                info['profiles'] = profiles

            yield info

        # Follow links (only relevant for start URL)
        for next_page in response.css('a'):
            link = next_page.css('a::attr(href)').get()

            if link.startswith('/abgeordnete/biografien/'):
                full_link = urllib.parse.urljoin('https://bundestag.de', link)
                yield response.follow(full_link, self.parse)
