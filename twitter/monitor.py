import configparser
import tweepy
import json
import logging
import datetime

from twitter.utils import tweets_to_file


# Load settings
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read('config.ini')

log = logging.getLogger(__name__)


class TestListener(tweepy.StreamListener):
    """Listener to test functionality in development.

    Does not permanently store any information."""

    def __init__(self, *args, ignore_retweets=True, ignore_quote_tweets=True, **kwargs):
        super().__init__(*args, **kwargs)

        self.ignore_retweets = ignore_retweets
        self.ignore_quote_tweets = ignore_quote_tweets

    def process_status(self, status):
        log.info("Crawled tweet: %s"%status._json)
        #log.info(dir(status))

    def on_status(self, status):
        if self.ignore_retweets and 'retweeted_status' in status._json:
            return
        if self.ignore_quote_tweets and status.is_quote_status:
            return

        self.process_status(status)

    def on_error(self, error):
        log.error(error)


class FileListener(TestListener):
    """Listener that writes tweets to files in JSON format."""

    def process_status(self, status):
        """Save a given status to a file."""
        file_path = config['files']['tweets']

        # Have separate files for different periods
        today = datetime.date.today()
        file_path += f"_{today.year}_{today.month}_{today.day}"

        file_path += '.json'

        #TODO would be good to also separate between users or keywords (could be two modes)
        tweets_to_file([status], file_path, writemode='a')
        #with open(file_path, 'a') as f:
        #    f.write(json.dumps(status._json)+'\n')


def connect(listener):
    """Connect to the Twitter API."""
    auth = tweepy.OAuthHandler(config['twitter_auth']['consumer_key'],
                                config['twitter_auth']['consumer_secret'])
    auth.set_access_token(config['twitter_auth']['access_token'],
                            config['twitter_auth']['access_token_secret'])
    api = tweepy.API(auth)

    stream = tweepy.Stream(auth=api.auth, listener=listener)
    return stream

def track_keywords(keywords, listener=FileListener(), **kwargs):
    """Obtain tweets including the given keywords.

    Arguments:
    - keywords: List of keywords to consider"""
    stream = connect(listener)

    stream.filter(track=keywords, **kwargs)
    # can by run asynchronously: myStream.filter(track=['python'], is_async=True)

def track_users(users, listener=FileListener(), **kwargs):
    """Obtain tweets from the given users.

    Arguments:
    - users: List of users to follow"""
    stream = connect(listener)

    stream.filter(follow=users, **kwargs)

