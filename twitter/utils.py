import json


def tweets_to_file(tweets, filename, writemode='a'):
    """Writes tweets to a file, one tweet per line.

    The file will contain the tweets in JSON format.

    Arguments:
    - tweets: Tweets to write to the file, as list of tweepy Status objects
    - filename: Filename as string (including the path)"""
    with open(filename, writemode) as f:
        for tweet in tweets:
            # Note that tweepy returns tweets as Status objects!
            f.write(json.dumps(tweet._json)+'\n')


def tweets_from_file(filename):
    tweets = [json.loads(line.strip()) for line in open(filename, 'r') if len(line.strip())>0]
    return tweets
    
    
def tweet_fields_from_file(filename, field='text'):
    texts = [json.loads(line.strip())[field] for line in open(filename, 'r') if len(line.strip())>0 and field in json.loads(line.strip()).keys()]
    return texts


def media_from_tweet(tweet, filter_=None):
    """Extract a list of all media URLs from the given tweet.

    Arguments:
    - tweet: single tweet in JSON format"""
    if 'media' not in tweet['entities'].keys():
        return []
    contained_media = tweet['entities']['media']
    if filter_ is not None:
        return [media['media_url_https'] for media in filter(filter_,contained_media)]
    else:
        return [media['media_url_https'] for media in contained_media]


def media_from_tweets(tweets, type_=None):
    """Extract a list of all media URLs from the given tweets.

    Arguments:
    - tweets: List of tweets in JSON format
    - type_: If not None, only media of this type will be extracted. Give type_ in string format as used by twitter API. (E.g. "photo" for images)"""
    urls = set()
    for tweet in tweets:
        if type_ is not None:
            urls |= set(media_from_tweet(tweet, filter_=lambda m: m['type']==type_))
        else:
            urls |= set(media_from_tweet(tweet))
    return urls

