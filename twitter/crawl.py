import tweepy
import json
import logging
from tweepy import OAuthHandler
import time
import os
import configparser

from general.download import download_media
from twitter.utils import *


# Load settings
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read('config.ini')

log = logging.getLogger(__name__)


#TODO don't print anything but use logging instead

def search_tweets(query, out_file, max_tweets=5000, oldest_id=None, newest_id=None):
    """Crawl tweets for a search query and write them to a file
    in JSON format (one tweet per line).

    :param query: String to search for in tweet texts
    :param out_file: Path of the file to use as output
    :param max_tweets: Maximal number of tweets to obtain for each user.
    :param oldest_id: If given, no tweets with ID less than oldest_id will be crawled.
    :param newest_id: If given, no tweets with ID larger than newest_id will be crawled.
    """
    log.info("Initializing API ...")
    auth = tweepy.OAuthHandler(config['twitter_auth']['consumer_key'],
                                config['twitter_auth']['consumer_secret'])
    auth.set_access_token(config['twitter_auth']['access_token'],
                            config['twitter_auth']['access_token_secret'])
    api = tweepy.API(auth, wait_on_rate_limit=True)

    print("Crawling tweets for query '%s' ..." % query)
    try:
        tweets_to_file(get_query_tweets(api, query, max_tweets=max_tweets, newest=newest_id, oldest=oldest_id), out_file)

    except (KeyboardInterrupt, SystemExit):
        raise
    except Exception as e:
        log.error("Exception of type %s while processing query '%s'!"%(str(type(e)),query))
        log.error(str(e))

    print("Finished crawling.")

def get_query_tweets(api, query, max_tweets=5000, batch_size=500, newest=None, oldest=None, **kwargs):
    """Search tweets by query."""
    alltweets = []

    while max_tweets>0:
        new_tweets = api.search(q=query, result_type='recent', count=min(batch_size,max_tweets), since_id=oldest, max_id=newest, **kwargs)

        max_tweets -= len(new_tweets)
        alltweets.extend(new_tweets)
        log.info("...%d tweets downloaded so far"%len(alltweets))

        if len(new_tweets)<1:
            # i.e. we read all tweets for this query
            break

        # save id of oldest tweet less one (so only older tweets will be obtained in next iteration)
        newest = alltweets[-1].id - 1

    return alltweets


def get_user_tweets(api, screen_name=None, user_id=None, max_tweets=5000, batch_size=500, newest=None, oldest=None):
    """Read tweets from the timeline of a given user.

    Note: This code is based on https://gist.github.com/yanofsky/5436496

    :param screen_name: Screen name of the user. If this is None, then user_id must specify the user's ID.
    param user_id: Twitter ID of the user.
    :param max_tweets: Maximal number of tweets to receive in total
    :param batch_size: Number of statuses to retrieve within one batch
    :returns: Tweets as tweepy Status objects (can be converted to JSON by calling method ._json)"""
    assert screen_name is not None or user_id is not None, "Either screen_name or user_id is required!"
    alltweets = []

    while max_tweets>0:
        # get batch_size most recent tweets of this user
        #FIXME: check if it actually works to pass None for min_id or max_id
        if user_id is not None:
            new_tweets = api.user_timeline(user_id=user_id, count=min(batch_size,max_tweets), since_id=oldest, max_id=newest)
        else:
            new_tweets = api.user_timeline(screen_name=screen_name, count=min(batch_size,max_tweets), since_id=oldest, max_id=newest)

        max_tweets -= len(new_tweets)
        alltweets.extend(new_tweets)
        log.info("...%d tweets downloaded so far"%len(alltweets))

        if len(new_tweets)<1:
            # i.e. we read all tweets from this user
            break

        # save id of oldest tweet less one (so only older tweets will be obtained in next iteration)
        newest = alltweets[-1].id - 1

    return alltweets


def crawl_users(users, tweet_dir, max_tweets=5000, skip_existing=True, oldest_id=None, newest_id=None):
    """Crawl tweets of the given users and write them to txt files
    in JSON format (one tweet per line).

    :param users: list of user screen names
    :param tweet_dir: directory that will be used to store the data, in string format
    :param max_tweets: Maximal number of tweets to obtain for each user.
    :param skip_existing: If True, crawling is skipped for users who already have an existing txt file in the tweet folder. If False, the old file is overwritten.
    :param oldest_id: If given, no tweets with ID less than oldest_id will be crawled.
    :param newest_id: If given, no tweets with ID larger than newest_id will be crawled.
    """
    log.info("%d users to crawl"%len(users))

    log.info("Initializing API ...")
    auth = tweepy.OAuthHandler(config['twitter_auth']['consumer_key'],
                                config['twitter_auth']['consumer_secret'])
    auth.set_access_token(config['twitter_auth']['access_token'],
                            config['twitter_auth']['access_token_secret'])
    api = tweepy.API(auth, wait_on_rate_limit=True)

    for user in users:
        if any(type(user)==t for t in [tuple, list]):
            id_,screen_name = user
        else:
            screen_name = user

        raw_tweet_file = os.path.join(tweet_dir, '%s.json'%screen_name)

        # Checking how many tweets of this user we already have
        if os.path.isfile(raw_tweet_file) and skip_existing:
            continue

        #print("Crawling tweets of user %s (ID %d) ..."%(screen_name,id_))
        log.info("Crawling tweets of user %s ..."%screen_name)
        try:
            tweets_to_file(get_user_tweets(api, screen_name, max_tweets=max_tweets, newest=newest_id, oldest=oldest_id), raw_tweet_file)

        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception as e:
            log.error("Exception of type %s while processing user '%s'!"%(str(type(e)),screen_name))
            log.error(str(e))
            continue

    print("Finished crawling.")


def fuse_txt_files(folder, outfile):
    log.info("Fusing files into %s ..."%outfile)
    with open(outfile, 'w') as f_out:
        is_first = True

        for root, dirs, files in os.walk(folder):
            for name in files:
                if not name.endswith('.txt'):
                    continue

                log.info("Processing file %s ..."%name)
                with open(os.path.join(root, name), 'r') as f_in:
                    if is_first:
                        f_out.write(f_in.read())
                        is_first = False
                    else:
                        f_out.write(f_in.read().split('\n',1)[1]) # first line is header
                f_out.write('\n')
    log.info("Done.")


def download_images_for_tweet_directory(max_users, tweet_dir, media_path):
    """Download all images for tweets from a specific directory.

    Note that this assumes tweets in JSON format and in the current version
    loads them all into working memory."""
    tweets = []
    for root, dirs, files in os.walk(tweet_dir):
        for i,file_ in enumerate(files):
            tweets.extend(tweets_from_file(os.path.join(root,file_)))
            if i>max_users:
                break

    #FIXME make more efficient by not loading all tweets into WM but only keeping the multimedia information
    log.info("Downloading media ...")
    download_media(media_from_tweets(tweets, type_='photo'),media_path)


def download_images_for_tweet_file(tweet_file):
    """Download all images for tweets in the given file.

    The images will be downloaded to a directory with the same name as the
    tweet file."""
    tweets = tweets_from_file(tweet_file)
    #FIXME: not very efficient since all tweets are read into working memory
    log.info("%d tweets read."%len(tweets))

    media_path = tweet_file.split('.',1)[0]+'/'
    if not os.path.exists(media_path):
        os.makedirs(media_path)
 
    log.info("Downloading media ...")
    download_media(media_from_tweets(tweets),media_path)

