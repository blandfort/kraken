# Interesting keywords to consider

emotional = [
        'sadness', 'depression', 'lonely', 'loneliness',
        'joy', 'love',
        'hatred', 'angry',
        'grief',
        'surprise',
        'shame',
        'disgusting', 'disgust',
        'deep',
        ]
emotional_hashtags = ['#'+word for word in emotional]
