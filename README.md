# Kraken

_Fancy tools for data collection._


## Set Up

- Clone the repo.
- `cp config.example.ini config.ini`
- Adjust settings in `config.ini`
- Create virtual environment: `python -m venv venv`
- Activate virtual environment: `source venv/bin/activate`
- Install dependencies: `pip install -r requirements.txt`
- Have fun.


## Cool Things You Can Do

Scraping twitter data:

- Scrape tweets of a given list of twitter users or based on a query (see `test_crawler.py` for an example)
- Follow a list of keywords or users (see `test_monitor.py` for an example)
- Media from scraped tweets can be downloaded using functions in `twitter/utils.py` and `general/download.py`

Scrape other websites:

- `test_spider.py` describes an example of scraping information about members of the German parliament from the official parliament website, using the python library scrapy
- `test.py` contains code to download images that appear on a given website
- Note: Code in `websites/` is preliminary and still needs to be cleaned up.

