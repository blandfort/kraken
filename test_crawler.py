import configparser

from twitter.crawl import crawl_users, search_tweets


# Load settings
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read('config.ini')

if __name__ == '__main__':
    import logger

    #users = ['@realDonaldTrump', '@DonaldJTrumpJr']
    #crawl_users(users, max_tweets=10000, tweet_dir=config['files']['tweet_dir'])

    search_tweets(query='test', max_tweets=150, out_file='files/twitter/test.json')
