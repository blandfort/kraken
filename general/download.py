import os
import urllib
import logging
import configparser


# Load settings
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read('config.ini')

log = logging.getLogger(__name__)


def download_media(urls, base_path=config['files']['media_dir'], overwrite=False):
    for url in urls:
        log.info("Downloading %s"%url)
        filename = os.path.join(base_path, url.split('/')[-1])

        if not overwrite and os.path.isfile(filename):
            continue

        try:
            urllib.request.urlretrieve(url, filename)

            # updating mapping from original urls to file on disk
            with open(config['files']['url_mapping'], 'a') as f:
                f.write('%s\t%s\n' % (url, os.path.abspath(filename)))
        except KeyboardInterrupt:
            break
        except Exception as e:
            # log the exception
            with open(os.path.join(base_path, 'errors.log'), 'a') as f:
                f.write(url+'\n')
                f.write(str(type(e))+'\n')
                f.write(str(e)+'\n\n')
            with open(os.path.join(base_path, 'failed_urls.log'), 'a') as f:
                f.write(url+'\n')

